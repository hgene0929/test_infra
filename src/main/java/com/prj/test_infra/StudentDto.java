package com.prj.test_infra;

import lombok.Data;

@Data
public class StudentDto {

	private String name;

	public Student toEntity() {
		return new Student(name);
	}
}
