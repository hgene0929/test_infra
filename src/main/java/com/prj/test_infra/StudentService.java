package com.prj.test_infra;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class StudentService {

	private final StudentRepository studentRepository;

	public void save(StudentDto dto) {
		studentRepository.save(dto.toEntity());
	}

	public StudentDto get(long id) {
		Student student = studentRepository.findById(id)
			.orElseThrow(() -> new IllegalArgumentException("not-found"));

		StudentDto dto = new StudentDto();
		dto.setName(student.getName());

		return dto;
	}
}
