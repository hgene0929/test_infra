package com.prj.test_infra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestInfraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestInfraApplication.class, args);
	}

}
