package com.prj.test_infra;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class StudentApi {

	private final StudentService studentService;

	@PostMapping("/students")
	public ResponseEntity<Void> save(StudentDto dto) {
		studentService.save(dto);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@GetMapping("/students/{id}")
	public ResponseEntity<StudentDto> get(@PathVariable long id) {
		StudentDto dto = studentService.get(id);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}
}
